import pandas as pd
from luminovo.data_models.inventory_and_offers import *
from luminovo.data_models.components import *

DEFAULT_mapping = {
    "manufacturer": "Hersteller",
    "ipn": "IPN",
    "mpn": "MPN",
    "description": "Comment",
    "cpn": "Customer Part Number",
    "customer": "Customer",
    "customer_id": "Customer Nummer",
    "preferred": "Preferred",
}


def parse_selectline_components(
    raw_data: pd.DataFrame, mapping: dict[str, str] | None = None
) -> list[Component]:
    """ADAPT this function to suit it best to your data! It takes the raw data
    and maps it to the Luminovo data model

    Args:
        raw_data (pd.DataFrame): the raw data
        mapping (dict[str, str]): the mapping of your raw data to Luminovo column names


    Returns:
        list[Component]: a list containing objects of the type Component. It should
            include all components you want to import to Luminovo.
    """

    if not mapping:
        mapping = DEFAULT_mapping

    components = {}
    for _, row in raw_data.iterrows():
        manufacturer = row[mapping["manufacturer"]]
        mpn = row[mapping["mpn"]]
        row_ipn = str(row[mapping["ipn"]])
        form_fit_name = row[mapping["form_and_fit_name"]]
        if form_fit_name:
            form_fit = FormAndFit(name=row[mapping["form_and_fit_name"]])
        else:
            form_fit = None

        if row_ipn not in components:
            if mpn and manufacturer != "generic":
                part = ManufacturerPartNumber(manufacturer=manufacturer, mpn=mpn)
                data = ComponentDataOffTheShelfSpec(
                    parts=[part],
                    specification=Specification(form_and_fit=form_fit),
                )
                component = ComponentSpec(data=data, type="OffTheShelf")
                ipn = InternalPartNumber(value=row_ipn)
                description = row[mapping["description"]]
                single_component = Component(
                    component=component,
                    description=description,
                    internal_part_number=ipn,
                )
            else:
                part = ManufacturerPartNumber(manufacturer=manufacturer, mpn=mpn)
                data = ComponentDataOffTheShelfSpec(
                    parts=[part],
                    specification=Specification(type="Generic", form_and_fit=form_fit),
                )
                component = ComponentSpec(data=data, type="OffTheShelf")
                ipn = InternalPartNumber(value=row_ipn)
                description = row[mapping["description"]]
                single_component = Component(
                    component=component,
                    description=description,
                    internal_part_number=ipn,
                )

            components[row_ipn] = single_component
        else:
            if mpn:
                additional_part = ManufacturerPartNumber(
                    manufacturer=row[mapping["manufacturer"]], mpn=row[mapping["mpn"]]
                )
                components[row_ipn].component.data.parts.append(additional_part)

    return list(components.values())


def parse_selectline_inventory(
    raw_data: pd.DataFrame, mapping: dict[str, str] | None = None
) -> list[OfferDetails]:
    """ADAPT this function to suit it best to your data! It takes the raw data
    and maps it to the Luminovo data model

    Args:
        raw_data (pd.DataFrame): the raw data
        mapping (dict[str, str]): the mapping of your raw data to Luminovo column names

    Returns:
        list[OfferDetails]: a list containing objects of the type OfferDetails. It should
            include all Offers you want to import to Luminovo.
    """
    if not mapping:
        mapping = DEFAULT_mapping

    raw_data = raw_data.drop_duplicates(subset=[mapping["ipn"]])

    offers = []
    for _, row in raw_data.iterrows():
        row_ipn = str(row[mapping["ipn"]])
        available_stock = row[mapping["available_stock"]]

        if not available_stock:
            available_stock = 0
        else:
            available_stock = int(available_stock)

        unit_price = str(row[mapping["unit_price"]])
        currency = str(row[mapping["currency"]])
        if currency == "None":
            currency = "EUR"

        offer = OfferDetails(
            availability=Availability(
                available_stock=available_stock,
            ),
            part=InternalPartNumberInventory(row_ipn),
            prices=[
                Prices(
                    unit_price=UnitPrice(amount=unit_price, currency=currency),
                )
            ],
            supplier=Supplier(type="Internal"),
        )
        offers.append(offer)

    return offers
