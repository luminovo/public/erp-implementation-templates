# This module uses the selecline connector to get the data provided
# through the SelectLineERP macro, packages this data to Luminovo
# API format and sends it to Luminovo.

import sys
import os

# manipulate the directory so that the modules can be imported
current_dir = os.path.dirname(os.path.abspath(__file__))
repo_dir = os.path.abspath(os.path.join(current_dir, ".."))
sys.path.insert(0, repo_dir)
sys.path.insert(0, os.path.abspath(os.path.join(repo_dir, "..")))

from connectors import selectline
import examples.selectline.data_processor as data_processor
from luminovo.api import api as lumi_api

import dotenv

dotenv.load_dotenv()

hostname = os.getenv("SELECTLINE_HOSTNAME")
username = os.getenv("SELECTLINE_USERNAME")
password = os.getenv("SELECTLINE_PASSWORD")
app_key = os.getenv("SELECTLINE_APPKEY")
macro_name = os.getenv("MACRO_NAME")


if not (hostname and username and password and app_key and macro_name):
    print(hostname, username, password, app_key)
    raise ValueError(
        "Environmental variables for the hostname, username, passowrd, app_key or macro_name for seletline access are missing"
    )
token, token_type = selectline.get_token(hostname, username, password, app_key)
data = selectline.get_macro(hostname, token, token_type, macro_name)
data = selectline.convert_macros_response_to_table(data)

# This is the mapping of columns for a particular macro in selectline.
# Should be adapted for every new selectline implementation!
selectline_mapping = {
    "ipn": "eigene Artikelnummer",
    "description": "Artikelbezeichnung",
    "form_and_fit_name": "_GEHAEUSE",
    "Artikelgruppe": "Artikelgruppe",
    "manufacturer": "Type",
    "mpn": "HerstellerArtikelNr",
    "currency": "Waehrung",
    "unit_price": "letzter EK",
    "available_stock": "Lagerbestand",
    "supplier_type": "Supplier Type",
}

components_data = data_processor.parse_selectline_components(data, selectline_mapping)
inventory_data = data_processor.parse_selectline_inventory(
    data, mapping=selectline_mapping
)

client_id = os.getenv("LUMINOVO_CLIENT_ID")
client_secret = os.getenv("LUMINOVO_CLIENT_SECRET")
luminovo_tenant = os.getenv("LUMINOVO_TENANT_NAME")

if client_id and client_secret and luminovo_tenant:
    token = lumi_api.request_bearer_token(client_id, client_secret)
    lumi_api.import_components(luminovo_tenant, token, components_data)
    lumi_api.import_inventory_and_offers(luminovo_tenant, token, inventory_data)
