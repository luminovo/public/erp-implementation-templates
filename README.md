This is a collection of templates which can be used to build up an API connection to Luminovo

# How-To
- Use luminovo data models to build up a request to Luminovo API
- Use connectors to build the data retrieval and data transformation steps from your ERP system to Luminovo. Right now available modules are:
    - Parsing excel files
    - SelectLine ERP macro retrieval