"""
Module: data_procerssor.data_processor
Description: This module implements a basic parser of raw data and packages the data into Luminovo
    data models. 
    NOTE: The parsing and the mapping have to be adjusted for every new implementation depending
    on the data structure!
Author: Igor Rzhin
Date: 2024-26-06
"""

import pandas as pd
from luminovo.data_models.inventory_and_offers import *
from luminovo.data_models.components import *


# adapt the right side of the mapping to suit columns in your data
DEFAULT_mapping = {
    "manufacturer": "Hersteller",
    "ipn": "IPN",
    "mpn": "MPN",
    "description": "Comment",
    "cpn": "Customer Part Number",
    "customer": "Customer",
    "customer_id": "Customer Nummer",
    "preferred": "Preferred",
}


def parse_components(
    raw_data: pd.DataFrame, mapping: dict[str, str] | None = None
) -> list[Component]:
    """ADAPT this function to suit it best to your data! It takes the raw data
    and maps it to the Luminovo data model

    Args:
        raw_data (pd.DataFrame): the raw data
        mapping (dict[str, str]): the mapping of your raw data to Luminovo column names

    Returns:
        list[Component]: a list containing objects of the type Component. It should
            include all components you want to import to Luminovo.
    """

    if not mapping:
        mapping = DEFAULT_mapping

    components = {}
    for _, row in raw_data.iterrows():
        manufacturer = row[mapping["manufacturer"]]
        mpn = row[mapping["mpn"]]
        row_ipn = str(row[mapping["ipn"]])

        if row_ipn not in components:
            if mpn:
                part = ManufacturerPartNumber(manufacturer=manufacturer, mpn=mpn)
                data = ComponentDataOffTheShelfSpec(parts=[part])
                component = ComponentSpec(data=data, type="OffTheShelf")
                ipn = InternalPartNumber(value=row_ipn)
                customer = Customer(number=row[mapping["customer"]])
                customer_part_number = CustomerPartNumber(
                    customer=customer, value=mapping["cpn"]
                )
                description = row[mapping["description"]]
                single_component = Component(
                    component=component,
                    description=description,
                    internal_part_number=ipn,
                    customer_part_numbers=[customer_part_number],
                )
            else:
                part = ManufacturerPartNumber(
                    manufacturer=row[mapping["manufacturer"]], mpn=mpn
                )
                data = ComponentDataOffTheShelfSpec(
                    parts=[part], specification=Specification(type="Generic")
                )
                component = ComponentSpec(data=data, type="OffTheShelf")
                ipn = InternalPartNumber(value=row_ipn)
                description = row[mapping["description"]]
                single_component = Component(
                    component=component,
                    description=description,
                    internal_part_number=ipn,
                )

            components[row_ipn] = single_component
        else:
            if mpn:
                additional_part = ManufacturerPartNumber(
                    manufacturer=row[mapping["manufacturer"]], mpn=row[mapping["mpn"]]
                )
                components[row_ipn].component.data.parts.append(additional_part)

    return list(components.values())


def parse_inventory(
    raw_data: pd.DataFrame, mapping: dict[str, str] | None = None
) -> list[OfferDetails]:
    """ADAPT this function to suit it best to your data! It takes the raw data
    and maps it to the Luminovo data model

    Args:
        raw_data (pd.DataFrame): the raw data
        mapping (dict[str, str]): the mapping of your raw data to Luminovo column names

    Returns:
        list[OfferDetails]: a list containing objects of the type OfferDetails. It should
            include all Offers you want to import to Luminovo.
    """

    if not mapping:
        mapping = DEFAULT_mapping

    offers = []
    for _, row in raw_data.iterrows():
        row_ipn = str(row[mapping["ipn"]])
        available_stock = row[mapping["available_stock"]]
        total_stock = row[mapping["total_stock"]]
        lead_time = row[mapping["lead_time"]]

        if not available_stock:
            available_stock = 0
        else:
            available_stock = int(available_stock)
        if total_stock is not None:
            total_stock = int(total_stock)
        if lead_time is not None:
            lead_time = int(lead_time)

        unit_price = str(row[mapping["unit_price"]])
        currency = str(row[mapping["currency"]])
        if currency == "None":
            currency = "EUR"
        if currency == "RMB":
            currency = "CNY"
        stock_location = str(row[mapping["stock_location"]])
        moq = row[mapping["moq"]]
        mpq = row[mapping["mpq"]]

        if moq == 0.0:
            moq = None
        if mpq == 0.0:
            mpq = None

        offer = OfferDetails(
            availability=Availability(
                available_stock=available_stock,
                lead_time=lead_time,
                total_stock=total_stock,
            ),
            part=InternalPartNumberInventory(row_ipn),
            prices=[
                Prices(
                    unit_price=UnitPrice(amount=unit_price, currency=currency),
                    moq=moq,
                    mpq=mpq,
                )
            ],
            supplier=Supplier(supplier=stock_location, type="Internal"),
        )
        offers.append(offer)

    return offers
