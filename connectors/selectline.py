"""
Module: selectline
Description: Connect to the SelectLine API using this module
Author: Igor Rzhin
Date: 2024-26-06
"""

import requests
import pandas as pd
import numpy as np


def get_token(
    hostname: str, username: str, password: str, app_key: str
) -> tuple[str, str]:

    request_url = f"https://{hostname}/login"

    headers = {"username": username, "password": password, "AppKey": app_key}
    resp = requests.post(url=request_url, json=headers, timeout=20, verify=False)

    if resp.status_code != 200:
        raise ValueError(
            f"The API Auth request resulted in an error. Response: {resp.text}"
        )
    access_token = resp.json().get("AccessToken")
    token_type = resp.json().get("TokenType")
    return access_token, token_type


def get_macro(hostname: str, access_token: str, token_type: str, macro_name) -> dict:
    request_url = f"https://{hostname}/macros/{macro_name}"

    headers = {"Authorization": f"{token_type} {access_token}"}
    # Adapt the body if additional parameters to the macro are required
    body = [{"Name": "", "Value": ""}]

    resp = requests.post(
        url=request_url, json=body, headers=headers, timeout=20, verify=False
    )

    if resp.status_code != 200:
        raise ValueError(
            f"The API Auth request resulted in an error. Response: {resp.text}"
        )
    data = resp.json()
    return data


def convert_macros_response_to_table(raw_response: dict) -> pd.DataFrame:
    columns = raw_response["ColumnNames"]
    rows = [data["ColumnValues"] for data in raw_response["Rows"]]
    df = pd.DataFrame(data=rows, columns=columns)
    df.replace({pd.NA: None, np.nan: None}, inplace=True)
    return df
