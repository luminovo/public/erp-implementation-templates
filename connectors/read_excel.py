import pandas as pd


def open_excel(path: str, header_row: int) -> pd.DataFrame:
    """Open excel and replace all empty cells with a None
    Args:
        path (str): the path to the .xlsx file
        header_row (int): the number of the header row where the data starts
    Returns:
        pd.DataFrame: the content of the excel file
    """
    df = pd.read_excel(path, na_values="", header=header_row)
    df.replace({pd.NA: None}, inplace=True)
    return df
