import requests
import json
from luminovo.data_models.components import Component
from luminovo.data_models.inventory_and_offers import OfferDetails


def request_bearer_token(client_id: str, client_secret: str) -> str:
    """From the Luminovo API, request the bearer token.

    Args:
        client_id (str): the client id
        client_secret (str): the client secret

    Raises:
        ValueError: if no bearer token is returned, a ValueError is raised.

    Returns:
        str: the bearer token
    """
    request_body = {
        "audience": "https://luminovo.ai/api",
        "client_id": client_id,
        "client_secret": client_secret,
        "grant_type": "client_credentials",
    }
    response = requests.post(
        url="https://luminovo.eu.auth0.com/oauth/token",
        data=request_body,
        timeout=20,
    )
    bearer = response.json().get("access_token")

    if not bearer:
        raise ValueError(f"Authentication failed with the response: {response.text}")

    return bearer


def import_components(tenant_name: str, bearer: str, components: list[Component]):
    components = [component.asdict() for component in components]
    with open("components_export.json", "w") as f:
        json.dump(components, f, indent=4)
    components_upload_url = f"https://{tenant_name}.luminovo.com/api/components/import"

    headers = {"Authorization": f"Bearer {bearer}"}
    resp = requests.post(
        url=components_upload_url, json=components, headers=headers, timeout=20
    )
    if resp.status_code != 202:
        raise ValueError(f"The API request resulted in an error. Response: {resp.text}")
    else:
        print("The API request was successful!")
        print(f"API Response: {resp.text}")


def import_inventory_and_offers(
    tenant_name: str, bearer: str, offers: list[OfferDetails]
):
    offers = [offer.asdict() for offer in offers]
    with open("offers_export.json", "w") as f:
        json.dump(offers, f, indent=4)
    offers_upload_url = f"https://{tenant_name}.luminovo.com/api/offers/import"

    headers = {"Authorization": f"Bearer {bearer}"}
    resp = requests.post(
        url=offers_upload_url, json=offers, headers=headers, timeout=20
    )
    if resp.status_code != 207:
        raise ValueError(f"The API request resulted in an error. Response: {resp.text}")

    for part_response in resp.json():
        if part_response["status"] != 200:
            print(f"Failed: {part_response}")
