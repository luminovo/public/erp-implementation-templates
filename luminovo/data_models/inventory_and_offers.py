from dataclasses import dataclass
from luminovo.data_models.dataclass_to_dict import asdict_without_none


@dataclass
class OnOrder:
    quantity: int
    date: str | None = None


@dataclass
class Availability:
    available_stock: int
    lead_time: int | None = None
    total_stock: int | None = None
    on_order: list[OnOrder] | None = None


@dataclass
class CustomerInventory:
    number: str
    name: str | None = None


@dataclass
class InternalPartNumberInventory:
    internal_part_number: str


@dataclass
class UnitPrice:
    amount: str
    currency: str


@dataclass
class Prices:
    unit_price: UnitPrice
    moq: int | None = None
    mpq: int | None = None


@dataclass
class Supplier:
    type: str
    supplier: str | None = None
    supplier_part_number: str | None = None
    supplier_number: str | None = None


@dataclass
class UnitOfMeasurement:
    quantity: float
    unit: str


@dataclass
class OfferDetails:
    availability: Availability
    part: InternalPartNumberInventory
    prices: list[Prices]
    customer: CustomerInventory | None = None
    supplier: Supplier | None = None
    unit_of_measurement: UnitOfMeasurement | None = None
    packaging: str | None = None
    valid_until: str | None = None

    def asdict(self) -> dict:
        """Overwrites the standard asdict method with a custom method removing
        from the dict all keys which have None as values (hence, removing all not-filled
        out data fields)

        Returns:
            dict: the component as a dictionary
        """
        return asdict_without_none(self)
