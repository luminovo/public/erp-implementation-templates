from dataclasses import dataclass
from luminovo.data_models.dataclass_to_dict import asdict_without_none


@dataclass
class ManufacturerPartNumber:
    mpn: str
    manufacturer: str | None = None


@dataclass
class FormAndFit:
    name: str
    mounting: str | None = None
    number_of_pins: int | None = None


@dataclass
class FunctionData:
    power_rating_watts: str = ""
    resistance_ohms: str = ""
    temperature_coefficient_ppmk: str = ""
    tolerance_percent: str = ""
    voltage_rating_volts: str = ""


@dataclass
class Function:
    data: FunctionData
    type: str


@dataclass
class Specification:
    type: str = "BoundToMPNs"
    form_and_fit: FormAndFit | None = None
    function: Function | None = None
    restricted_manufacturers: list[str] | None = None


@dataclass
class Customer:
    number: str
    name: str | None = None


@dataclass
class ComponentDataOffTheShelfSpec:
    parts: list[ManufacturerPartNumber] | None = None
    specification: Specification | None = None


@dataclass
class CustomPartsSpec:
    part_type: str
    description: str | None = None
    reach_compliant: str | None = None
    rohs_compliant: str | None = None


@dataclass
class ComponentDataCustomSpec:
    custom_parts: CustomPartsSpec


@dataclass
class ComponentSpec:
    data: ComponentDataOffTheShelfSpec | ComponentDataCustomSpec
    type: str


@dataclass
class CustomerPartNumber:
    customer: Customer
    value: str
    revision: str | None = None


@dataclass
class InternalPartNumber:
    value: str
    revision: str | None = None


@dataclass
class Component:
    """The main data class representing one component. A typical upload
    to the api/components/import endpoint contains an array consisting of
    multiple components like this one - one for every electronical part in your PLM/EPR system.
    """

    internal_part_number: InternalPartNumber
    component: ComponentSpec | None = None
    customer_part_numbers: list[CustomerPartNumber] | None = None
    description: str | None = None
    is_preferred: bool | None = None
    is_restricted_to_customers: bool | None = None

    def asdict(self) -> dict:
        """Overwrites the standard asdict method with a custom method removing
        from the dict all keys which have None as values (hence, removing all not-filled
        out data fields)

        Returns:
            dict: the component as a dictionary
        """
        return asdict_without_none(self)
