from dataclasses import fields


def asdict_without_none(instance) -> dict:
    """Transfrom dataclass objects to dictionaries, but leaving out completely all None values"""

    if not hasattr(instance, "__dataclass_fields__"):
        return instance
    result = {}
    for field in fields(instance):
        value = getattr(instance, field.name)
        if value is None:
            continue
        if isinstance(value, list):
            result[field.name] = [
                (
                    asdict_without_none(item)
                    if hasattr(item, "__dataclass_fields__")
                    else item
                )
                for item in value
            ]
        else:
            result[field.name] = (
                asdict_without_none(value)
                if hasattr(value, "__dataclass_fields__")
                else value
            )
    return result
